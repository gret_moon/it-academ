package servlet;

import model.Rectangle;
import service.calculator.RectangleSquare;
import service.calculator.SquareDAOImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/f")
public class FindSquareServlet extends HttpServlet {

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        double a = Double.parseDouble(req.getParameter("a"));
        double b = Double.parseDouble(req.getParameter("b"));


        ServletContext servletContext = getServletContext();
        servletContext.setAttribute("a", a);
        servletContext.setAttribute("b", b);

        //calculate
        RectangleSquare square = new RectangleSquare();
        double result =  square.RectangleSquare(a,b);
        servletContext.setAttribute("result", result);

        //find session atr
        HttpSession session = req.getSession();
        String s = (String) session.getAttribute("login");

        //add to DB
        SquareDAOImpl squareDAOImpl = new SquareDAOImpl();
        squareDAOImpl.addRectangle(a,b,result,s);

        //read All data from DB
        ArrayList<Rectangle> rectangles = squareDAOImpl.readAll(s);
        req.setAttribute("rectangles", rectangles);


        servletContext.getRequestDispatcher("/pages/userPage.jsp").forward(req, resp);

    }
}
