 <%@ page contentType="text/html;charset=UTF-8" language="java" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

 <!DOCTYPE html>
 <html>
 <head>
     <title>Archive page</title>
 </head>

 <body>
<h2>Archive of the calculation: </h2> <br>

<blockquote>
<table border="1">
      <thead>
        <td><b> id </b></td>
        <td><b> a </b></td>
        <td><b> b </b></td>
        <td><b> Result </b></td>

        <td><b> . </b></td>
      </thead>

<c:forEach var="rectangle" items="${rectangles}">
      <tr>
        <td>${rectangle.id} </td>
        <td>${rectangle.a} </td>
        <td>${rectangle.b} </td>
        <td>${rectangle.result} </td>
        <td> <form method="POST" action='<c:url value="/delete" />' style="display:inline;">
            <input type="hidden" name="id" value="${rectangle.id}">
            <input type="submit" value="Delete">
        </form></td>
      </tr>
</c:forEach>

</table>
</blockquote>
      <p><input type="submit" value="Back to calculation"
                   onclick="window.location='pages/userPage.jsp'"/> </p>




 </body>
 </html>