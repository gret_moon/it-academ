package service;

public interface UserDAO {
    boolean create(String login, String password);
    boolean checkLog(String login);
    boolean logInTry (String login, String password);
    String checkRole(String login);
}
