package service.calculator;

import model.Rectangle;
import service.JDBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SquareDAOImpl {

    public ArrayList<Rectangle> readAll(String s) {
        ArrayList<Rectangle> rectangles = new ArrayList<>();
        try {
            Connection connection = JDBConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM public.\"squareData\" WHERE login=?");
            preparedStatement.setString(1, s);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                double a = resultSet.getDouble("a");
                double b = resultSet.getDouble("b");
                double result = resultSet.getDouble("result");
                int id = resultSet.getInt("id");
                rectangles.add(new Rectangle(a,b,result,id));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rectangles;
    }

    public void deleteRectangle(int id ) {
        try {
            Connection connection = JDBConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("DELETE FROM public.\"squareData\" WHERE id=? ");
            preparedStatement.setInt(1, id);

            preparedStatement.execute();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addRectangle(double a, double b, double result, String s) {
        try {
            Connection connection = JDBConnect.getConnection();
            PreparedStatement ps = connection.prepareStatement
                    ("INSERT INTO public.\"squareData\" (a, b, result, login)  VALUES(?,?,?,?)");
            ps.setDouble(1, a);
            ps.setDouble(2, b);
            ps.setDouble(3, result);
            ps.setString(4, s);

            ps.execute();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Rectangle> readAllAdmin() {
        ArrayList<Rectangle> rectanglesAll = new ArrayList<>();
        try {
            Connection connection = JDBConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT * FROM public.\"squareData\" ");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                double a = resultSet.getDouble("a");
                double b = resultSet.getDouble("b");
                double result = resultSet.getDouble("result");
                String login = resultSet.getString("login");
                rectanglesAll.add(new Rectangle(a,b,result,login));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rectanglesAll;
    }



}
