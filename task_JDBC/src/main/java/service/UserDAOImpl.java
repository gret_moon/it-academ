package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAOImpl implements UserDAO {

    @Override
    public boolean create(String login, String password) {
        PreparedStatement preparedStatement1 = null;
        try {
            Connection connection = JDBConnect.getConnection();
            preparedStatement1 = connection.prepareStatement
                    ("INSERT INTO users VALUES(?,?,?)");
            preparedStatement1.setString(1, login);
            preparedStatement1.setString(2, password);
            preparedStatement1.setString(3, "USER");
            preparedStatement1.execute();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
//        finally { JDBConnect.close();
//        }
        return true;
    }

    @Override
    public boolean checkLog(String login) {
        boolean result = false;
        PreparedStatement preparedStatement;
        try {
            Connection connection = JDBConnect.getConnection();
            preparedStatement = connection.prepareStatement
                    ("SELECT login FROM users WHERE login =?");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("login");
                if (name == null) {
                    result = false;
                } else {
                    result = true;
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
//        finally { JDBConnect.close();
 //       }
        return result;
    }

    @Override
    public boolean logInTry(String login, String password) {
        boolean result = false;
        PreparedStatement preparedStatement0 = null;

        try {
            Connection connection = JDBConnect.getConnection();
            preparedStatement0 = connection.prepareStatement
                    ("SELECT login FROM users WHERE login= ? AND password =?");
            preparedStatement0.setString(1, login);
            preparedStatement0.setString(2, password);
            ResultSet resultSet = preparedStatement0.executeQuery();
            while (resultSet.next()) {
                String login1 = resultSet.getString("login");
                if (login1 == null) {
                    result = false;
                } else {
                    result = true;
                }
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
//        finally { JDBConnect.close();}
        return result;
    }

    @Override
     public String checkRole(String login) {
        String result = null;
        try {
            Connection connection = JDBConnect.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement
                    ("SELECT role FROM public.users WHERE login =?");
            preparedStatement.setString(1, login);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                String role = resultSet.getString("role");
                result = role;
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        //finally { JDBConnect.close();}
        return result;
    }
}
