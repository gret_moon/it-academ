 <%@ page contentType="text/html;charset=UTF-8" language="java" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
 <%@ page import="java.util.List"%>
 <%@ page import="model.Rectangle"%>

 <!DOCTYPE html>
 <html>
 <head>
     <title>User page</title>
 </head>

 <body>
<h1>Calculate square of the rectangle </h1>
        <form action="f" method="GET" >
                       <p><b>a: </b><br>
            <input type="text" required placeholder="a" name="a" ></p>
                       <p><b>b: </b><br>
            <input type="text" required placeholder="b" name="b"></p>
        <p> <input class="button" type="submit" value="Confirm"></p>
        </form>

<a href="<c:url value="/data"/>">Archive</a>

<h2>Archive of the calculation: </h2> <br>

<blockquote>
<table border="1">
      <thead>
        <td><b> id </b></td>
        <td><b> a </b></td>
        <td><b> b </b></td>
        <td><b> Result </b></td>

        <td><b> . </b></td>
      </thead>

<c:forEach var="rectangle" items="${rectangles}">
      <tr>
        <td>${rectangle.id} </td>
        <td>${rectangle.a} </td>
        <td>${rectangle.b} </td>
        <td>${rectangle.result} </td>
        <td> <form method="POST" action='<c:url value="/delete" />' style="display:inline;">
            <input type="hidden" name="id" value="${rectangle.id}">
            <input type="submit" value="Delete">
        </form></td>
      </tr>
</c:forEach>

</table>
</blockquote>

<a href="<c:url value="/logout"/>"> Logout</a>


 </body>
 </html>

