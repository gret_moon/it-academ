package servlet.filter;

import model.User;
import service.UserDAO;
import service.UserDAOImpl;
import servlet.hash.PasswordCoder;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;


@WebFilter("/filter")
public class AuthFilter implements Filter {
    private final static Logger log = Logger.getLogger(servlet.RegistrationServlet
            .class.getName());
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String login = req.getParameter("login");
        String password = req.getParameter("password");

        PasswordCoder hash = new PasswordCoder();

        try {
            password  = hash.codePass(password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        HttpSession session = req.getSession();
        UserDAO user = new UserDAOImpl();

        /*get role*/
        session.setAttribute("login", login);
        String s = user.checkRole(login);


        if (user.logInTry(login, password)) {

           if (s.equalsIgnoreCase(String.valueOf(User.ROLE.USER))){
               moveToMenu(req, res, User.ROLE.USER);
               log.info("The user was logged-in :" + login);

           } else if (s.equalsIgnoreCase(String.valueOf(User.ROLE.ADMIN))){
               moveToMenu(req, res, User.ROLE.ADMIN);
               log.info("The admin was logged-in");

           }
        } else if (!user.logInTry(login, password)){
            moveToMenu(req, res, User.ROLE.UNKNOWN);
            log.info("The user entered the wrong data");

        }
    }

    private void moveToMenu(HttpServletRequest req,
                            HttpServletResponse res,
                            User.ROLE role)
            throws ServletException, IOException {

        if (role.equals(User.ROLE.UNKNOWN)) {
            req.getRequestDispatcher("pages/wrongLog.jsp").forward(req, res);
        } else if (role.equals(User.ROLE.USER)) {
            req.getRequestDispatcher("pages/userPage.jsp").forward(req, res);
        }else if (role.equals(User.ROLE.ADMIN)){
            req.getRequestDispatcher("/admin").forward(req, res);
        }
    }

    @Override
    public void destroy() {
    }

}
