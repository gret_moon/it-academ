package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JDBConnect {

private static String url = "jdbc:postgresql://localhost:5432/project";
private static Connection conn = null;

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user","postgres");
        props.setProperty("password","pass");

        try {
            Class.forName("org.postgresql.Driver");
            if (conn == null) {
                conn = DriverManager.getConnection(url, props);;
            }
        } catch (ClassNotFoundException | SQLException e) {
            throw new SQLException("Connection doesn't work ", e);
        }
        return conn;
    }

    public static void close() {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("Connection hasn't closed");
            }
        }
    }
}


