package servlet;

import service.calculator.SquareDAOImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")
public class deleteData extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        SquareDAOImpl squareDAOImpl = new SquareDAOImpl();
        float b = Float.parseFloat(request.getParameter("id"));
        int id = (int) b;

        squareDAOImpl.deleteRectangle(id);
        request.getRequestDispatcher("/pages/userPage.jsp").forward(request, response);

    }
}

