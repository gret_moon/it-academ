package servlet.tag;

import service.calculator.RectangleSquare;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class FindSquareTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        double high = Double.parseDouble(pageContext.getRequest().getParameter("high"));
        double length = Double.parseDouble(pageContext.getRequest().getParameter("length"));
        RectangleSquare calculator = new RectangleSquare();
        double areaRectangle = calculator.RectangleSquare(high, length);
        JspWriter out = pageContext.getOut();
        try {
            out.print("<h2>");
            out.print("The square is "+ areaRectangle);
            out.print("</h2>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
