package servlet;

import service.UserDAO;
import service.UserDAOImpl;
import servlet.hash.PasswordCoder;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
private final static Logger log = Logger.getLogger(servlet.RegistrationServlet
        .class.getName());

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
                throws ServletException, IOException {

            String login = req.getParameter("login");
            String password = req.getParameter("password");

            ServletContext servletContext = getServletContext();
            PasswordCoder hash = new PasswordCoder();

            servletContext.setAttribute("login", login);
            servletContext.setAttribute("password", password);

            try {
                 password  = hash.codePass(password);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
                log.warning("Error(hash code of the password) ");
            }


            UserDAO user = new UserDAOImpl();

            if (!user.checkLog(login)) {
                log.info("The user " + login +" was registered");
                user.create(login,password );
                servletContext.getRequestDispatcher("/index.jsp").forward(req, resp);
            } else if (user.checkLog(login)){
                log.info("The user entered existing login");
                servletContext.getRequestDispatcher("/pages/ExistLog.jsp").forward(req, resp);

            }

        }


}
