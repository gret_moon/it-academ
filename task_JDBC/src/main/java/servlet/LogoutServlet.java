package servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Logger;

public class LogoutServlet extends HttpServlet {
    private final static Logger log = Logger.getLogger(servlet.RegistrationServlet
            .class.getName());
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
        log.info("The user was logged-out");

        HttpSession session = req.getSession();
        session.removeAttribute("login");
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

}
