package model;

public class Rectangle {
    private double a;
    private double b;
    private double result;
    private int id;
    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Rectangle(double a, double b, double result) {
        this.a = a;
        this.b = b;
        this.result = result;
    }


    public Rectangle(double a, double b, double result, int id) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.id = id;
    }

    public Rectangle(double a, double b, double result, String login) {
        this.a = a;
        this.b = b;
        this.result = result;
        this.login = login;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getResult() {
        return result;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }
    public void setId(int id ) {
        this.id = id;
    }
    public double getId() {
        return id;
    }
    public void setResult(double result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return  a +
                "/" + b +
                "/" + result + "/" +id;
    }
}
