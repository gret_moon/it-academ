 <%@ page contentType="text/html;charset=UTF-8" language="java" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<title> Admin page </title>
</head>

<body>
<h1> Welcome to the admin page </h1>

<h2>Archive of the calculation: </h2> <br>
<blockquote>
<table border="1">
      <thead>
         <td><b> a </b></td>
        <td><b> b </b></td>
        <td><b> Result </b></td>
        <td><b> login </b></td>
        <td><b> . </b></td>
      </thead>

<c:forEach var="rectangle" items="${rectangles}">
      <tr>
        <td>${rectangle.a} </td>
        <td>${rectangle.b} </td>
        <td>${rectangle.result} </td>
        <td>${rectangle.login} </td>
        </form></td>
      </tr>
</c:forEach>

</table>
</blockquote>

<a href="<c:url value="/logout"/>"> Logout</a>

</body>
</html>