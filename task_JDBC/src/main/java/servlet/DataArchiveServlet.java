package servlet;

import model.Rectangle;
import service.calculator.SquareDAOImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

@WebServlet("/data")
public class DataArchiveServlet extends HttpServlet {
    private final static Logger log = Logger.getLogger(servlet.RegistrationServlet
            .class.getName());
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        ServletContext servletContext = getServletContext();
        SquareDAOImpl squareDAOImpl = new SquareDAOImpl();

        //find session atr
        HttpSession session = req.getSession();
        String s = (String) session.getAttribute("login");

        //read All data from DB
        ArrayList<Rectangle> rectangles = squareDAOImpl.readAll(s);
        req.setAttribute("rectangles", rectangles);
        log.info("Archive of the calculation was requested");


        servletContext.getRequestDispatcher("/pages/CalculArchive.jsp").forward(req, resp);

    }
}
