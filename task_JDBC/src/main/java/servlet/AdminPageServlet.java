package servlet;

import model.Rectangle;
import service.calculator.SquareDAOImpl;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

@WebServlet("/admin")
public class AdminPageServlet extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ServletContext servletContext = getServletContext();
        SquareDAOImpl squareDAOImpl = new SquareDAOImpl();

        //read All data from DB
        ArrayList<Rectangle> rectangles = squareDAOImpl.readAllAdmin();
        req.setAttribute("rectangles", rectangles);



        servletContext.getRequestDispatcher("/pages/adminPage.jsp").forward(req, resp);
    }
}
